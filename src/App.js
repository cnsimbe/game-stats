import React, { Component } from 'react';
import './App.css';

import { HomeDashBoard } from "./components/HomeDashBoard"

class App extends Component {
  render() {
    return <ion-app><HomeDashBoard / ></ion-app>;
  }
}

export default App;
