import React, { Component } from 'react';
import './HomeDashBoard.css';

import { DataApi } from "../../services/DataApi"
import { MatchListItem } from "../MatchListItem"

export class HomeDashBoard extends Component {

  constructor(props){
  	super(props)
  	this.state = { userName : null, matchList: [] }
  	this.searchInput = React.createRef()
  	this.submitBtn = React.createRef()
  }

  textChange = ($event)=> {
  	let value = $event.target.value
  	this.submitBtn.current.disabled = !value
  }


  searchSubmit = async ()=> {
  	let userName = this.searchInput.current.value
  	if(userName)
  	{
  		console.log("Search submitted : ", userName)
  		DataApi.getSummoner(userName)

  		this.setState({userName})
  	}
  }

  render() {
    return (
    	[<ion-header key="0">
    		<ion-toolbar>
   				<ion-title>League of Legends - Game Stats</ion-title>
  			</ion-toolbar>
    	</ion-header>,
    	<ion-content key="1">
    		<ion-item>
    			<ion-input onKeyUp={(e)=>e.keyCode===13 ? this.searchSubmit() : ''} ref={this.searchInput} onInput={this.textChange} slot="start" type="text" placeholder="Enter username for the summoner"></ion-input>
    			<ion-button ref={this.submitBtn} onClick={this.searchSubmit} slot="start" disabled="true">Get Matches</ion-button>
    		</ion-item>
    		{
	    		this.state.userName && 
	    		(<ion-item>
	    			<ion-label>Match list for {this.state.userName}</ion-label>
	    		</ion-item>)
    		}
    		{
    			this.state.matchList.map(match=><MatchListItem match={match} />)
    		}
    	</ion-content>
    	]
    )
  }
}
