import React, { Component } from 'react';
import './MatchListItem.css';

export class MatchListItem extends Component {
  render() {
  	let match = this.props.match;
    return (
      <ion-item>
      	<div>
      		<ion-note>{match.status.name}</ion-note>
      		<ion-note>{match.duration.text}</ion-note>
      	</div>
      	<div>
      		<img src={match.player.image} alt="{match.player.displayName}"/>
      		<ion-note>{match.player.displayName}</ion-note>
      	</div>
      	<div>
      		{
      			match.player.spells.map(spell=>(
      				<div>
      					<img src={spell.image} alt="{spell.description}"/>
      				</div>
      			))
      		}
      		
      	</div>
      	<div>
      		<div>
      			<ion-note></ion-note> /
      			<ion-note></ion-note> /
      			<ion-note></ion-note>
      		</div>
      		<div>
      			<ion-note>{match.KDA} : 1</ion-note>
      			<ion-note>KDA</ion-note>
      		</div>
      	</div>
      	<div>
  			<ion-note>{match.level}</ion-note>
  			<ion-note>{match.creep_score} ({match.creep_score_min}) CS</ion-note>
  			<ion-note>P/Kill {match.pkill} %</ion-note>
  			<ion-note>Match MMR {match.mmr}</ion-note>
      	</div>
      	<div>
      		{
      			match.items_bought.map(item=>(
      				<div>
      					<img src={item.image} alt="{item.description}"/>
      				</div>
      			))
      		}	
      	</div>
      </ion-item>
    );
  }
}
